forum_generate
--------------

This module extends devel_generate to allow example forum structures to be generated.

Drupal core's forums (including those enhanced by Advanced Forum) are really just taxonomy terms,
and the devel_generate module can generate taxonomy terms by itself.

What this module adds is the ability to create some terms which are forum containers,
plus it also specifically inserts generated terms into the appropriate vocabulary.

Who should use it?
==================

Developers might want to use it to create test data when working with core forum or Advanced Forum.

Similarly site builders could use it to create example data in order to evaluate Drupal's forum
functionality, including enhancements to this functionality added by other contrib modules.

Usage
=====

The forum_generate module adds an admin page to those already provided by devel_generate:

/admin/generate/forum

...hopefully the options there are self-explanatory.

If you want to create a set of test forum data, you might want to do something like:

* generate 100 users
* generate 5 forum containers and 25 forums
* generate 250 Forum topic nodes, with maximum 50 comments, adding taxonomy terms to each node.

The drush support in devel_generate is also extended by this module, so you could create the
example test forum data above with a set of drush commands something like this:

drush genu 100
drush genf 5 25
drush genc --add-terms --types=forum 250 50

Caution
=======

Warning - there is an option to delete the existing forum structure before generating a new one.
There is no undo for this, so be very careful.

You almost certainly do not need or want to have this module enabled on a live production site.
