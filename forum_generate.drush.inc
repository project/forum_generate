<?php

/**
 * @file
 *  Generate an example forum structure.
 */

/**
 * Implementation of hook_drush_help().
 */
function forum_generate_drush_help($section) {
  switch ($section) {
    case 'drush:generate-forums':
      return dt('Generate an example forum structure.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function forum_generate_drush_command() {
  $items['generate-forums'] = array(
    'description' => 'Creates a forum structure.',
    'arguments' => array(
      'num_containers' => 'Number of forum containers to generate.',
      'num_forums' => 'Number of forums to generate',
    ),
    'options' => array(
      'kill' => 'Delete all forums and containers before generating.'
    ),
    'aliases' => array('genf'),
  );
  return $items;
}

/**
 * Command callback. Generate a number of taxonomy.
 */
function drush_forum_generate_generate_forums($num_containers = NULL, $num_forums = NULL) {
  if (drush_generate_is_number($num_containers) == FALSE) {
    drush_set_error('forum_generate_INVALID_CONTAINERS', dt('Invalid number of containers.'));
  }
  if (drush_generate_is_number($num_forums) == FALSE) {
    drush_set_error('forum_generate_INVALID_NUM_FORUMS', dt('Invalid number of forums.'));
  }
  drush_generate_include_devel(); // required for devel_generate_word if nothing else
  drush_op('forum_generate_generate_forums', $num_containers, $num_forums, '12', drush_get_option('kill'));
  drush_log(dt('Generated @num_containers containers, @num_forums forums', array('@num_containers' => $num_containers, '@num_forums' => $num_forums)), 'ok');
}
